module.exports = {
  transpileDependencies: true,
  pluginOptions: {
    electronBuilder: {
      preload: 'src/preload.js',
      builderOptions: {
          productName: "Wifi model",
          appId: 'wifi_model',
          win: {
            'target': [
                'nsis'
            ],
            'icon': 'icon.ico',
          },
          'nsis': {
              'installerIcon': 'icon.ico',
              'uninstallerIcon': 'icon.ico',
              'uninstallDisplayName': 'Uninstall wifi model',
              'oneClick': false,
              'allowToChangeInstallationDirectory': true
          },

          extraResources: [
              './backend-build'
          ],
      },
    }
  },
  chainWebpack: config => {
    config
        .plugin('html')
        .tap(args => {
            args[0].title = 'Wifi model';
            return args;
        })
    }
}
