'use strict'

import { app, protocol, BrowserWindow } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS3_DEVTOOLS } from 'electron-devtools-installer'
const { ipcMain } = require('electron');
const path = require('path');
const isDevelopment = process.env.NODE_ENV !== 'production'
const net = require('net');
const spawn = require('child_process').spawn;
const execSync = require('child_process').execSync;


const isRunning = (query) => {
  let platform = process.platform;
  let cmd = '';
  switch (platform) {
    case 'win32' : cmd = `tasklist`; break;
    case 'darwin' : cmd = `ps -ax | grep ${query}`; break;
    case 'linux' : cmd = `ps -A`; break;
    default: break;
  }
  return execSync(cmd).toString().toLowerCase().indexOf(query.toLowerCase()) > -1;
}

const child = spawn(path.resolve(__dirname, '../backend-build/wifi_model.exe'));

if (isRunning('wifi_model.exe')) {
  console.log('wifi_model.exe has opened successfully');
}

protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])


const client = new net.Socket();

ipcMain.on('READ_FILE', (e, msg) => {
  if (!client.connected) {
    client.connect(3000, '127.0.0.1', function() {});
    client.connected = true
    client.on('close', function() {
      client.destroy();
    });
  }
  client.write(msg);
});

async function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
      preload: path.resolve(__dirname, 'preload.js'),
      title: 'Wifi model',
    }
  })
  // eslint-disable-next-line no-undef
  win.setIcon(path.join( __static, 'favicon.ico'))
  win.setMenu(null);
  if (process.env.WEBPACK_DEV_SERVER_URL) {
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    win.loadURL('app://./index.html')
  }

  client.on('data', function(data) {
    console.log('Received: ' + data);
    const frame = data.toString()
    win.webContents.send(frame.split(';')[0], data.toString())
  });
}

app.on('window-all-closed', () => {
  client.write('OUT');
  client.destroy();
  child.kill();
  if (isRunning('wifi_model.exe')) {
    console.error('wifi_model.exe has not closed successfully');
  }
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    try {
      await installExtension(VUEJS3_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        client.destroy();
        child.kill()
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      client.destroy();
      child.kill()
      app.quit()
    })
  }
}