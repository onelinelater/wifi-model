export const STATUSES = {
    CONNECTED: 'CONNECTED',
    DISCONNECTED: 'DISCONNECTED'
};

export const STATUS_LABEL = {
    [STATUSES.CONNECTED]: 'Подключён',
    [STATUSES.DISCONNECTED]: 'Отключён',
};

export const CSS_STATUS = {
    [STATUSES.CONNECTED]: 'success',
    [STATUSES.DISCONNECTED]: 'alert',
};

export const MESSAGE_TYPES = {
    INFO: 'INFO',
    ERROR: 'ERROR',
}

export const MESSAGE_COLORS = {
    [MESSAGE_TYPES.INFO]: 'info',
    [MESSAGE_TYPES.ERROR]: 'error',
}
