import {STATUSES} from "@/enums";

export const COMPUTERS_MOCK = [{
    name: 'Компьютер 1',
    macAddress: '00:26:57:00:1f:02',
    status: STATUSES.DISCONNECTED,
}, {
    name: 'Компьютер 2',
    macAddress: '00:31:51:00:2a:91',
    status: STATUSES.CONNECTED,
}, {
    name: 'Компьютер 3',
    macAddress: '05:94:3a:ee:19:a1',
    status: STATUSES.DISCONNECTED,
}, {
    name: 'Компьютер 4',
    macAddress: '41:ae:51:10:00:b2',
    status: STATUSES.DISCONNECTED,
}]
