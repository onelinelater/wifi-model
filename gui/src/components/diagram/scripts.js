import ComputerIconComponent from '@/components/computer-icon'
import RouterIconComponent from '@/components/router-icon'
import InternetIconComponent from '@/components/internet-icon'
import {STATUSES} from "@/enums";

export default {
    name: 'DiagramComponent',
    components: {
        ComputerIconComponent,
        RouterIconComponent,
        InternetIconComponent
    },
    props: {
        computers: {
            type: Array,
            default: [],
        },
    },
    data() {
        return {
            diagramWidth: 300,
            computerIconWidth: 40,
            routerIconWidth: 30,
            iconPaddingX: 30,
            iconPaddingY: 10,
        }
    },
    computed: {
        offset() {
            return (this.diagramWidth - (this.computerIconWidth + this.iconPaddingX) * this.computers.length) / 2
        },
        center() {
            return this.offset + (this.computers.length * (this.computerIconWidth + this.iconPaddingX)) / 2
        },
        computersOnline() {
            return this.computers.filter(this.isConnected)
        }
    },
    methods: {
        enumerate(items) {
            return items.map(item => {
                item.index = this.computers.indexOf(item)
                return item
            })
        },
        isConnected(item) {
            return item.status === STATUSES.CONNECTED
        }
    }
};