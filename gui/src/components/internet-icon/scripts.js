export default {
    name: 'InternetIcon',
    components: {},
    props: {
        width: {
            type: Number,
            default: 15,
        },
        height: {
            type: Number,
            default: 15,
        },
        x: {
            type: Number,
            default: 0,
        },
        y: {
            type: Number,
            default: 0,
        },
    },
};