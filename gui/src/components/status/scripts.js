import {CSS_STATUS, STATUS_LABEL} from "@/enums";


export default {
    name: 'StatusComponent',
    components: {},
    props: {
        status: {
            type: String,
        },
    },
    computed: {
        cssClasses() {
            return [
                CSS_STATUS[this.status]
            ]
        },
        text() {
            return STATUS_LABEL[this.status]
        }
    }
};