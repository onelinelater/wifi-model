import {MESSAGE_COLORS } from "@/enums";


export default {
    name: 'TerminalComponent',
    components: {},
    props: {
        status: {
            type: String,
        },
        log: {
            type: Array,
            default: []
        },
    },
    methods: {
        cssClass (type) {
            return [
                MESSAGE_COLORS[type]
            ]
        },
        formatDate(datetime) {
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            return (
                `${zeroPad(datetime.getDay(), 2)}.`+
                `${zeroPad(datetime.getMonth() + 1, 2)}.`+
                `${datetime.getFullYear()} ` +
                `${zeroPad(datetime.getHours(), 2)}:` +
                `${zeroPad(datetime.getMinutes(), 2)}`)
        }
    }
};