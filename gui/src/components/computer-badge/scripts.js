import IconComponent from '../icon'
import StatusComponent from '../status'
import {STATUSES} from "@/enums";


export default {
    name: 'ComputerBadge',
    components: {
        IconComponent,
        StatusComponent,
    },
    props: {
        status: {
            type: String,
            default: STATUSES.DISCONNECTED,
            validator: value => Object.values(STATUSES).includes(value),
        },
        name: {
            type: String,
            default: 'Неопознанный компьютер',
        },
        macAddress: {
            type: String,
            default: 'Error',
        },
    },
    computed: {
        isConnected() {
            return this.status === STATUSES.CONNECTED
        }
    },
    methods: {
        wifi() {
            if (this.status === STATUSES.DISCONNECTED) {
                window.ipc.send('READ_FILE', `CONNECT;${this.macAddress};`);
            } else {
                window.ipc.send('READ_FILE', `DISCONNECT;${this.macAddress};`);
            }
        },
        sendRequest() {
            window.ipc.send('READ_FILE', `SEND;${this.macAddress};`);
        },
    }
};