import socket
import time

if __name__ == '__main__':
    HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
    PORT = 3000  # Port to listen on (non-privileged ports are > 1023)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        msg = f"LOAD;BEGIN;"
        s.sendall(msg.encode('utf-8'))
        time.sleep(2)
        s.send(b"")
        print('Data send')
        msg = s.recv(12)
        while(msg):
            print(msg)
            msg = s.recv(12)

        s.close()



