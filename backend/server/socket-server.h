#ifndef WIFI_MODEL_SOCKET_SERVER_H
#define WIFI_MODEL_SOCKET_SERVER_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <string>
#include <vector>
#include <map>


class Request {
public:
    Request() {
        this->length = 0;
        this->raw_data = std::string("");
    }

    std::string raw_data;
    std::vector<std::string> tokens;
    int length;

private:
};


class Response {
public:
    Response() {
        this->length = 0;
        this->data = std::string("");
    }

    std::string data;
    int length;

private:
};


class SocketServer {
    public:
        SocketServer() {
            this->start_win_sock();
            this->socket_object = this->create_socket();
            this->bind_port();
            this->listen_socket();
            printf ("Wait for incoming connections...\n");
        }
        ~SocketServer() {
            closesocket(this->socket_object);
            this->stop_win_sock();
        }
        std::map<std::string, Response* (*)(Request *a)> handlers;
        void serve_forever();
        void add_handler(std::string, Response* (*)(Request *a));
        Response* process_request(Request *);

    private:
        SOCKET	socket_object;
        void stop_win_sock();
        void start_win_sock();
        SOCKET create_socket();
        void bind_port();
        void listen_socket();
        void parse_frame(Request *req, char *recvbuf, int msg_len);
        void process_messages();
};


#endif
