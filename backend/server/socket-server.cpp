#include "socket-server.h"
#include <iostream>

#define QUEUE_SIZE	1
#define MSG_LEN		128
#define PORT_ADDRESS 3000
#include <sstream>
#include <vector>
#define DIVIDER	';'



void SocketServer::stop_win_sock() {
    WSACleanup();
}

void SocketServer::start_win_sock() {
    WSADATA ws;
    if (FAILED (WSAStartup (MAKEWORD (1, 1) , &ws))) {
        this->stop_win_sock();
        exit(1);
    }
}


SOCKET SocketServer::create_socket() {
    SOCKET socket_object = socket (AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (this->socket_object == INVALID_SOCKET) {
        this->stop_win_sock();
        exit(1);
    }
    return socket_object;
}

void SocketServer::bind_port() {
    sockaddr_in	sock_bind;
    ZeroMemory (&sock_bind, sizeof (sock_bind));
    sock_bind.sin_family = AF_INET;
    sock_bind.sin_addr.S_un.S_addr = htonl (INADDR_ANY);
    sock_bind.sin_port = htons (PORT_ADDRESS);

    if (SOCKET_ERROR == bind(this->socket_object, (sockaddr* ) &sock_bind, sizeof (sock_bind) ) ) {
        closesocket(this->socket_object);
        this->stop_win_sock();
        exit(1);
    }
}

void SocketServer::listen_socket() {
    int errors = listen(this->socket_object , QUEUE_SIZE);
    if (errors == SOCKET_ERROR) {
        closesocket(this->socket_object);
        this->stop_win_sock();
        exit(1);
    }
}


void SocketServer::parse_frame(Request *req, char *recvbuf, int msg_len) {
    std::stringstream raw(req->raw_data + std::string(recvbuf));
    std::string token;
    while(std::getline(raw, token, DIVIDER)) {
        req->tokens.push_back(token);
    }
}


Response* SocketServer::process_request(Request *req) {
    std::string command = req->tokens[0];
    std::cout << " " << command << " ";
    if (this->handlers.count(command) != 0) {
        Response* (*func)(Request *) = this->handlers[command];
        return func(req);
    } else {
        return new Response();
    }
}


void SocketServer::process_messages() {
    SOCKET new_connection;
    sockaddr_in new_socket_address;
    char recvbuf[MSG_LEN];
    int new_len = sizeof (new_socket_address);
    ZeroMemory (&new_socket_address, sizeof (new_socket_address));

    new_connection = accept(socket_object, (sockaddr* ) &new_socket_address, &new_len);
    bool stay_connected = true;
    auto req = new Request();

    while (SUCCEEDED (new_connection) && stay_connected) {
        std::cout << "Client " << inet_ntoa ((in_addr) new_socket_address.sin_addr) << ":" << ntohs (new_socket_address.sin_port);
        int msg_len;
        int total = 0;

        msg_len = recv (new_connection, (char* ) &recvbuf, MSG_LEN, 0);
        if (msg_len <= 0) {
            break;
        }
        this->parse_frame(req, recvbuf, msg_len);

        if (req->tokens[0] == "OUT") {
            stay_connected = false;
        }

        Response *res = this->process_request(req);
        std::cout << " OK " << std::endl;
        total = 0;
        do {
            std::string frame = res->data.substr(total, MSG_LEN);
            msg_len = send(new_connection, frame.c_str(), frame.size(), 0);
            total += msg_len;
        } while(total < res->length);

        req->tokens.clear();
        delete res;
    }

    delete req;


    shutdown(new_connection, SD_SEND);
    closesocket(new_connection);
}

void SocketServer::serve_forever() {
    int i;
    while (i < QUEUE_SIZE) {
        i++;
        this->process_messages();
        i--;
    }
}

void SocketServer::add_handler(std::string name, Response* (*hdl)(Request *)) {
    if (this->handlers.count(name) == 0) {
        this->handlers[name] = hdl;
    }
}
