#ifndef WIFI_MODEL_ENTITIES_H
#define WIFI_MODEL_ENTITIES_H

#include <string>
#include <map>
#include <ctime>
#include <utility>

class WifiResponse {
public:
    WifiResponse(std::string site_name, int content_size) {
        this->site_name = std::move(site_name);
        this->content_size = content_size;
    }

    std::string site_name;
    int content_size;
    std::string serialize();

private:
};
class Computer {
public:
    Computer(std::string name, std::string mac_address) {
        this->name = std::move(name);
        this->mac_address = std::move(mac_address);
        this->status = std::string("DISCONNECTED");
    }

    std::string name;
    std::string mac_address;
    std::string status;
    std::string serialize();

private:
};

class Router {
public:
    Router(std::string mac_address) {
        std::srand(std::time(nullptr));
        this->connected = {};
        this->mac_address = std::move(mac_address);
    }
    ~Router() {
        this->connected.clear();
    }

    std::map<std:: string, Computer*> connected;
    std::string mac_address;
    void connect_computer(Computer* connected);
    void disconnect_computer(Computer* connected);
    WifiResponse* emit_response();
    std::string serialize();

private:
};

#endif
