#include "entities.h"
#include <vector>
#include <string>
# define MAX_WIFI_RESPONSE_SIZE 1024

void Router::connect_computer(Computer* computer) {
    computer->status = std::string("CONNECTED");
    this->connected[computer->mac_address] = computer;
}

void Router::disconnect_computer(Computer* computer) {
    computer->status = std::string("DISCONNECTED");
    this->connected.erase(computer->mac_address);
}

WifiResponse* Router::emit_response() {
    std::vector<std::string> HOST_NAMES {
        "https://pikabu.ru/",
        "https://hh.ru/",
        "https://linkedin.com/",
        "https://stackoverflow.com/",
        "https://www.google.com/",
        "https://habr.com/",
        "https://en.cppreference.com/"
    };
    int random_variable = std::rand() % HOST_NAMES.size();
    return new WifiResponse(HOST_NAMES[random_variable], std::rand() / MAX_WIFI_RESPONSE_SIZE);
}

std::string WifiResponse::serialize() {
    return std::string("WifiResponse") + ";" + this->site_name + ";" + std::to_string(this->content_size) + ";";
}

std::string Router::serialize() {
    return std::string("Router") + ";" + this->mac_address + ";";
}

std::string Computer::serialize() {
    return std::string("Computer") + ";" + this->mac_address + ";" + this->name + ";" + this->status + ";";
}
