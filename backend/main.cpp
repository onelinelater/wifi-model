#include "./server/socket-server.h"
#include "entities.h"
#include "handlers.h"
#include "dao.h"
int add(int x, int y)
{
    return x + y;
}

int invoke(int x, int y, int (*func)(int, int))
{
    return func(x, y);
}

int main(int argc, char* argv[]) {
    auto *server = new SocketServer();
    server->add_handler("LOAD", &load_handler);
    server->add_handler("CONNECT", &connect_handler);
    server->add_handler("DISCONNECT", &disconnect_handler);
    server->add_handler("SEND", &send_handler);
    server->serve_forever();

    delete server;
    delete dao;

    return 0;
}
