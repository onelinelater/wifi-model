#ifndef WIFI_MODEL_HANDLERS_H
#define WIFI_MODEL_HANDLERS_H
#include "./server/socket-server.h"

Response* load_handler(Request *);
Response* send_handler(Request *);
Response* connect_handler(Request *);
Response* disconnect_handler(Request *);

#endif
