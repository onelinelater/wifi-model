#include <algorithm>
#include "handlers.h"
#include "entities.h"
#include "dao.h"

Response* load_handler(Request *req) {
    auto res = new Response();
    for(const auto& computer: dao->computers) {
        res->data += "LOAD;" + computer->serialize();
    }
    res->data += "LOAD;END;";
    res->length = res->data.size();
    return res;
};
Response* send_handler(Request *req) {
    auto c = dao->computers;
    auto mac = req->tokens[1];
    auto it = std::find_if(
            c.begin(),
            c.end(),
            [&mac] (Computer * obj) { return obj->mac_address == mac; }
    );
    if(it != c.end()) {
        if ((*it)->status != "CONNECTED") {
            auto err = new Response();
            err->data = "SEND;ERROR;" + mac + ";";
            err->length = err->data.size();
            return err;
        }
        WifiResponse* wifi_res = dao->router->emit_response();
        auto res = new Response();
        res->data = "SEND;" + wifi_res->serialize() + mac + ";";
        res->length = res->data.size();
        delete wifi_res;

        return res;
    }

    return new Response();
}

Response* connect_handler(Request *req) {
    auto c = dao->computers;
    auto mac = req->tokens[1];
    auto it = std::find_if(
        c.begin(),
        c.end(),
        [&mac] (Computer * obj) { return obj->mac_address == mac; }
    );
    if(it != c.end()) {
        dao->router->connect_computer(*it);
        auto* res = new Response();
        res->data += "UPDATE;" + (*it)->serialize();
        res->length = res->data.size();

        return res;
    }

    return new Response();
};
Response* disconnect_handler(Request *req) {
    auto c = dao->computers;
    auto mac = req->tokens[1];
    auto it = std::find_if(
            c.begin(),
            c.end(),
            [&mac] (Computer * obj) { return obj->mac_address == mac; }
    );
    if(it != c.end()) {
        dao->router->disconnect_computer(*it);
        auto* res = new Response();
        res->data += "UPDATE;" + (*it)->serialize();
        res->length = res->data.size();

        return res;
    }

    return new Response();
};
