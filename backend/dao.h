#ifndef WIFI_MODEL_DAO_H
#define WIFI_MODEL_DAO_H
#include <string>
#include <vector>
#include "entities.h"


class Dao {
public:
    Dao() {
        this->computers = std::vector<Computer *>();
        this->computers.push_back(new Computer("Компьютер 1", "00:26:57:00:1f:02"));
        this->computers.push_back(new Computer("Компьютер 2", "00:31:51:00:2a:91"));
        this->computers.push_back(new Computer("Компьютер 3", "05:94:3a:ee:19:a1"));
        this->computers.push_back(new Computer("Компьютер 4", "41:ae:51:10:00:b2"));
        this->router = new Router("41:ae:51:10:00:b2");
    }

    ~Dao() {
        for (const auto &value: this->computers) {
            delete value;
        }
        this->computers.clear();
        delete this->router;
    }

    std::vector<Computer *> computers;
    Router *router;
};

extern Dao* dao;

#endif
